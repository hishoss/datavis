from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render, reverse
from django.views.generic import DetailView, RedirectView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin


from connection.models import Connection
from .models import User


class UserDetailView(LoginRequiredMixin, DetailView):
    model = User
    # These next two lines tell the view to index lookups by username
    slug_field = 'username'
    slug_url_kwarg = 'username'
    template_name = 'users/detail.html'

    def get_context_data(self, **kwargs):
        # from IPython import embed; embed()
        context = super(UserDetailView, self).get_context_data(**kwargs)
        context['cons'] = Connection.objects.filter(user=self.object)
        return context


class UserRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse('users:detail',
                       kwargs={'username': self.request.user.username})


class UserUpdateView(LoginRequiredMixin, UpdateView):

    fields = ['name', ]

    # we already imported User in the view code above, remember?
    model = User

    # send the user back to their own page after a successful update
    def get_success_url(self):
        return reverse('users:detail',
                       kwargs={'username': self.request.user.username})

    def get_object(self, **kwargs):
        # Only get the User record for the user making the request
        return User.objects.get(username=self.request.user.username)


# class UserListView(LoginRequiredMixin,  ListView):
#     model = User
#     # These next two lines tell the view to index lookups by username
#     slug_field = 'username'
#     slug_url_kwarg = 'username'


@login_required
def user_list_view(request):
    if request.user.is_staff:
        return render(request, 'users/list.html', {'user_list': User.objects.exclude(is_staff=True)})
    else:
        messages.warning(request, 'Access denied')
        return redirect(request.user)


# class UserChartListView(LoginRequiredMixin, ListView):
#     context_object_name = 'charts'
#     template_name = 'users/user_charts.html'
#
#     slug_field = 'username'
#     slug_url_kwarg = 'username'
#
#     def get_queryset(self):
#         return Chart.objects.filter(table__source_db__user__username=self.kwargs['username'])
#
#     def get_context_data(self, **kwargs):
#         context = super(UserChartListView, self).get_context_data(**kwargs)
#         context['username'] = self.kwargs['username']
#         return context


@login_required
def user_home_view(request):
    return render(request, 'users/detail.html', {"cons": Connection.objects.filter(user=request.user),
                                                 "object": request.user})
