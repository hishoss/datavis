from django.contrib.auth.models import AbstractUser
from django.shortcuts import reverse
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from package.models import Package

from .validators import phone_regex


def user_image_path(instance, filename):
    return f'{instance.username}/img/{instance.username}' + '.jpg'


@python_2_unicode_compatible
class User(AbstractUser):

    # First Name and Last Name do not cover name patterns
    # around the globe.
    name = models.CharField(_('Name of User'), blank=True, max_length=255)
    phone = models.CharField(validators=[phone_regex], max_length=17, blank=False,
                             verbose_name='Phone number', unique=True)
    image = models.ImageField(upload_to=user_image_path, null=True, blank=True)
    # organisation = models.ForeignKey(Organisation, null=True, blank=True, related_name='organisation', on_delete=models.CASCADE)
    # package = models.ForeignKey(Package, null=True, related_name='user', on_delete=models.CASCADE)

    def __str__(self):
        return self.username

    def get_absolute_url(self):
        return reverse('users:detail', kwargs={'username': self.username})

    # TODO: take this out later
    @property
    def package(self):
        return self.organisation.package


class Organisation(models.Model):
    TYPE_CHOICES = (
        ('agric', 'Agriculture'),
        ('finance', 'Finance'),
        ('education', 'Education'),
        ('it', 'I.T'),
        ('manufacturing', 'Manufacturing'),
        ('telecom', 'Telecommunication'),
    )
    name = models.CharField(_("Name of organisation"), max_length=255)
    phone = models.CharField(validators=[phone_regex], max_length=17, blank=False,
                             verbose_name='Phone number', unique=True)
    type = models.CharField(max_length=10, choices=TYPE_CHOICES)
    address = models.CharField(max_length=500)
    image = models.ImageField(upload_to=user_image_path, null=True, blank=True)
    users = models.ManyToManyField(User, blank=True)
    package = models.ForeignKey(Package, null=True, related_name='user', on_delete=models.CASCADE)
    updated = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
