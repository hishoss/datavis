from django.contrib.auth import get_user_model
from django import forms


class SignupForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ('phone', 'image')

    def signup(self, request, user):
        user.phone = self.cleaned_data['phone']
        user.image = self.cleaned_data['image']
        user.save()
