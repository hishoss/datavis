from datetime import timedelta

from django.db import models


class Package(models.Model):
    DURATION_CHOICES = (
        (31, 'One month'),
        (93, 'Three months'),
        (186, 'Six months'),
        (366, 'One year'),
    )
    FREQUENCY_CHOICES = (
        (1, '1 minute'),
        (5, '5 minutes'),
        (15, '15 minutes'),
        (30, '30 minutes'),
        (60, '1 hour'),
        (360, '6 hours'),
        (720, '12 hours'),
        (1440, '1 day'),
    )
    name = models.CharField(max_length=255)
    # Package.objects.filter(duration__gte=datetime.timedelta(days=20, hours=10))
    # duration = models.DurationField()
    duration = models.PositiveSmallIntegerField(choices=DURATION_CHOICES, default=1)
    frequency = models.PositiveSmallIntegerField(choices=FREQUENCY_CHOICES, default=1440)
    description = models.TextField()
    updated = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    @property
    def duration_delta(self):
        return timedelta(days=int(self.duration))

    @property
    def frequency_delta(self):
        return timedelta(minutes=int(self.frequency))

    # def get_absolute_url(self):
    #     return reverse('package:detail', kwargs={'pk': self.pk})
