from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect, render
from connection.terms_and_conditions import connection_terms
from .forms import PackagePurchaseForm
from .models import Package


@login_required
def package_list_view(request):
    return render(request, 'package/list.html', {'packages': Package.objects.all()})


@login_required
def package_purchase(request, pk):
    package = get_object_or_404(Package, pk=pk)

    form = PackagePurchaseForm(request.POST or None)
    if form.is_valid():
        if form.cleaned_data['agree']:
            request.user.package = package
            request.user.save()
            messages.success(request, f'Successfully subscribed to {package.name}')
            return redirect(request.user)
        else:
            messages.warning(request, "Package could not be purchased")
            return redirect("package:list")
    else:
        form = PackagePurchaseForm()
    return render(request, 'package/purchase.html', {'package': package, 'form': form,
                                                     'terms': connection_terms})
