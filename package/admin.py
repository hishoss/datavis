from django.contrib import admin
from .models import Package


class PackageAdmin(admin.ModelAdmin):
    list_display = ("name", "timeout", "duration", "description")
    list_filter = ('duration', 'timeout', 'timestamp')



admin.site.register(Package)

