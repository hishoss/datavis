from django.urls import path

from . import views

app_name = 'package'

urlpatterns = [
    path('', views.package_list_view, name='list'),
    path('<int:pk>/purchase/', views.package_purchase, name='purchase'),
]