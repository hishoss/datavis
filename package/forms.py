from django import forms


class PackagePurchaseForm(forms.Form):
    agree = forms.BooleanField(required=False)

