from django.conf import settings
from django.conf.urls import include
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.mixins import LoginRequiredMixin
# from django.contrib.auth.views import login
from django.urls import path
from django.views import defaults as default_views
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView

from graphene_django.views import GraphQLView
from users.views import user_home_view


class PrivateGraphQLView(LoginRequiredMixin, GraphQLView):
    pass


urlpatterns = [
    path('', user_home_view, name='user_home'),
    path('home/', TemplateView.as_view(template_name='pages/home.html'), name="home"),
    path('about/', TemplateView.as_view(template_name='pages/about.html'), name='about'),
    # path('admin/', admin.site.urls),  # set a honeypot on this
    path(settings.ADMIN_URL, admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path('connection/', include('connection.urls', namespace='connection')),
    path('dashboard/', include('dashboard.urls', namespace='dashboard')),
    path('graphql/', csrf_exempt(GraphQLView.as_view(graphiql=True))),
    path('package/', include('package.urls', namespace='package')),
    path('users/', include('users.urls', namespace='users')),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path('400/', default_views.bad_request, kwargs={'exception': Exception('Bad Request!')}),
        path('403/', default_views.permission_denied, kwargs={'exception': Exception('Permission Denied')}),
        path('404/', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}),
        path('500/', default_views.server_error),
    ]
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar
        urlpatterns = [
            path('__debug__/', include(debug_toolbar.urls)),
        ] + urlpatterns
