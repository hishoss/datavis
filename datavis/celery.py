import os
from django.conf import settings

from celery import Celery
from celery.schedules import crontab


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'datavis.settings')

app = Celery('datavis')

app.config_from_object('django.conf.settings', namespace='CELERY')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

# app.conf.beat_schedule = {
#     'pull-every-minute-crontab1': {
#         'task': 'blah',
#         'schedule': crontab(),  # crontab(hour=7, minute=30, day_of_week=1)
#         'args': ('a', 3),
#
#     },
#     'pull-every-minute-crontab2': {
#         'task': 'connection.tasks.add',
#         'schedule': 5.0,  # in seconds
#         'args': ('a', 3),
#
#     },
#     'add-every-monday-morning': {
#         'task': 'blah',
#         'schedule': crontab(hour=8, day_of_week=1),
#         'args': ('a', 3),
#     }
# }


@app.task(bind=True)
def debug_task(self):
    print("Request: {0!r}".format(self.request))