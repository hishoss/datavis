import graphene
from graphene_django.debug import DjangoDebug

import connection.schema  # like this so we know where the schema is coming from because several apps may have a schema
import chart.schema


class Query(chart.schema.Query, graphene.ObjectType):
    debug = graphene.Field(DjangoDebug, name='__debug')


schema = graphene.Schema(query=Query)
