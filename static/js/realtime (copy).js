
// function getData(maximum, arr, value) {
//     // value is a single number, data is an array of arrays with first
//     // element of each subarray as an index
//     // this function has to be extended to handle the case when the incoming
//     // data is a list or series of lists
//     let val = parseFloat(value);
//     if (arr.length === 0){
//         return [[0, val]];
//     }
//
//     let new_arr = arr.map(function(el, i){
//         return el[1];
//     });
//
//     new_arr.push(val);
//     new_arr = (new_arr.length > maximum) ? new_arr.slice(new_arr.length-maximum): new_arr;
//
//     new_arr.map(function(e, i){
//         return [i, e];
//         });
//
//     return new_arr;
// }


// function dateVal(arr) {
//     o = 100;
//     let res = arr.map(function(e, i){
//         return [new Date().getTime() + i*o, e];
//         });
//     return res;
// }

// copied from https://www.thecodeship.com/web-development/alternative-to-javascript-evil-setinterval/
function interval(func, wait, times){
    let interv = function(w, t){
        return function(){
            if(typeof t === "undefined" || t-- > 0){
                setTimeout(interv, w);
                try{
                    func.call(null);
                }
                catch(e){
                    t = 0;
                    throw e.toString();
                }
            }
        };
    }(wait, times);

    setTimeout(interv, wait);
}


// ---------------------------------------------------------------------------------------------------------------------


// easiest way to create a 2d array of [index, val]. We start with -1 so nothing is displayed on screen asuming y-axis starts from 0
let maximum = 20;
let zero = [...Array(maximum).keys()].map(i => [i, -1]);
let labels = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ,10];


// apparently, chart.js 2.x doesn't have automatic color assignment.
// Hence a convenience funtion to set this since the purpose of this is to generate dynamic charts.
// Plus this will also make all graphs share the same colors
let colors = palette('cb-Spectral', 10).map(
    function(hex) {
        return '#' + hex;
    });
        



// Chart.js Chart1

let canvas1 = $('#chart-1');

    
let data = {
  labels: labels,
  datasets: [{
      label: "aa",
      fill: false,
      lineTension: 0.0,
      backgroundColor: colors, //"rgba(75,192,192,0.4)",
      borderColor: "rgba(91,84,84,1)",
      data: [] //zero // seems data has to first be in place and successive data has to be of the same size
    }]
};


let option = {
  showLines: true,
  animation: {duration: 2},
  scales: {
    yAxes: [{
      display: true,
      ticks: {
        beginAtZero:true,
        min: 0,
        max: 100  
      }
    }]
  }
};

let chart1 = Chart.Line(canvas1,{
  data: data,
  options: option
});



// Chart.js 2

let canvas2 = document.getElementById("chart-2").getContext('2d');
let chart2 = new Chart(canvas2, {
    type: 'bar',
    data: {
        labels: ["One", "Two", "Three", "Four", "Five", "Six"],
        datasets: [{
            label: 'Polls',
            data: [0,0,0,0,0,0],
            backgroundColor: colors,
            borderColor: colors,
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});



// Smoothie.js Chart

let smoothie = new SmoothieChart({
  // interpolation: 'linear',
  // millisPerPixel: 60,
  grid: { strokeStyle:'rgba(119,119,119,0.29)', fillStyle:'rgb(00, 0, 0)',
          lineWidth: 1, 
          millisPerLine: 250,
          verticalSections: 6
        },
  labels: { fillStyle:'rgb(60, 0, 0)' },
  timestampFormatter: SmoothieChart.timeFormatter,
  maxValue:100,
  minValue:-1
});

// Data
let tSeries = new TimeSeries();

// Add to SmoothieChart
smoothie.addTimeSeries(tSeries, { strokeStyle:'rgb(0, 255, 0)',  lineWidth:3 });




let canvas4 = $("#chart-4");
let chart4 = new Chart(canvas4, {
    type: 'line',
    data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [{
            label: 'Revenue',
            data: [0,0,0,0,0,0,0,0,0,0,0,0],
            backgroundColor: 'green',
            borderColor: 'red',
            borderWidth: 1,
            fill: false,
        }]
    },
    options: {
        responsive: true,
        hover: {
            mode: 'nearest',
            intersect: true,
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        title: {
            display: true,
            text: 'Revenue',
        },
        scales: {
            yAxes: [{
                //gridLines: {display: false},
                ticks: {
                    beginAtZero:true
                }
            }],
            xAxes: [{
                gridLines: {display: false}
            }],
        },
        animation: {
            // onProgress: function (animation) {
            //     progress.value = animation.animationObject.currentStep / animation.animationObject.numSteps;
            // }
            numSteps: 10,
        }
    }
});







// --------------------------------------------------------------------------------------------------------------------------------------------
let ws_type = window.location.protocol === "https:" ? "wss:" : "ws:";
let ws_path = ws_type + "//" + window.location.host + ''; // last parameter is for resource location

const webSocketBridge = new channels.WebSocketBridge();
webSocketBridge.connect(ws_path);


webSocketBridge.socket.onopen = function (message) {
    console.log("Connected to data server");
    // automatically starting the stream. But a button will be ideal to start this process
    webSocketBridge.send({action: 'more', status: 'Charting data...'});

    // when there is no more data, wait for some time. If inactivity persists for some predefined time, disconnect
    // webSocketBridge.send({action: 'close'});
};


webSocketBridge.socket.onclose = function () {
    console.log("Disconnected from data server");
    //webSocketBridge.send({action: 'close'});
};

let j = 11;
let i = 0;
webSocketBridge.listen(function(message, stream){
// stream is like a sub-channel, so I guess this will be useful when using a multiplex
    let chart1_data = message.chart1_data; // .data because our consumer sends {'data': val}
    let chart2_data = message.chart2_data;
    let chart4_data = message.chart4_data;

    console.log(chart4_data);


    let c2 = chart2.data.datasets[0].data.map(function(val, i){
            return val + chart2_data[i];
        });


    // let listt = dateVal(data[2]);

    // smh. simple append
    chart1.data.datasets[0].data.push.apply(chart1.data.datasets[0].data, chart1_data);

    interval(function() {
        chart1.data.labels.push(j);
        chart1.data.labels.splice(0, 1);
        chart1.data.datasets[0].data.splice(0, 1);
        chart2.data.datasets[0].data = c2;
        chart4.data.datasets[0].data = chart4_data;



        $("#messages")[0].innerText = message.comments; // change this to messages
        $("#orders")[0].innerText = message.orders;
        $("#tasks")[0].innerText = message.tasks;
        $("#tickets")[0].innerText = message.tickets;

        chart1.update();
        chart2.update();
        chart4.update()


        tSeries.append(new Date().getTime(), chart1_data[i]);
        smoothie.streamTo(document.getElementById("chart-3"), 2000 /*delay*/);

        j++;
        i++;
    }, 500, chart1_data.length + 10);


    //
    // setInterval(function(){
    //     chart1.data.labels.push(j);
    //     chart1.data.labels.splice(0, 1);
    //     chart1.data.datasets[0].data.splice(0, 1);
    //     chart1.update();
    //     j++;
        // Smoothie.js
        // smoothie.js automatically splices data from head of list
        //tSeries.append(listt[0]);
        //tSeries.append(new Date().getTime(), data[0]);

        // delay on how much time to spend before plotting begins. If enough points/
        // are not received before plotting, there will be some jumps, so it's good
        // choice to start after some predefined time
        //smoothie.streamTo(document.getElementById("chart-4"), 2000 /*delay*/);
//     }, 1000);
});

// --------------------------------------------------------------------------------------------------------------------------------------------

   // $("#taskform").on("submit", function(event) {
   //     let message = {
   //         action: "start_sec3",
   //         job_name: $('#task_name').val()
   //     };
   //     socket.send(JSON.stringify(message));
   //     $("#task_name").val('').focus();
   //     return false;
   // });

