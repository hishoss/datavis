$(document).ready(function(){
    function renderChart(id, name, type, data, labels){
        let ctx = $("#" + id);
        new Chart(ctx, {
            type: type,
            data: {
                labels: labels,
                datasets: [{
                    label: name,
                    data: data,
                    backgroundColor: "rgba(0, 30, 150, 0.4)",
                    borderColor: "rgba(0, 30, 150, 1)",
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                backgroundColor: "rgba(60, 180, 180, 1)"
            }
        });
    }

    function fetchData(id, chart_pk){
         $.ajax({
             url: '/graphql/',
             contentType: 'application/json',
             method: 'POST',
             data: JSON.stringify({
                 'query': `query {
                     chart (id: ${chart_pk}) {
                         name
                         type
                         data
                         labels
                     }
                 }`
             }),
             success: function(response){
                 let data = response.data.chart;
                 renderChart(id, data.name, data.type.toLowerCase(), data.data, data.labels)
             },
             error: function(error){
                 // $.alert("An error occurred")
                 console.log(error)
             }
         })
    }

    let charts = $('.live-chart');
    $.each(charts, function(){
        let $this = $(this);
        if ($this.attr("id") && $this.attr("data-type")){
            fetchData($this.attr("id"), $this.attr("data-type"))
        }
    });
});


    //
    //  $('button').click(function() {
    //   event.preventDefault();
    //   let entry = $('#entry').val();
    //   console.log(entry);
    //
    //   $.ajax({
    //       method: "POST",
    //       url: "https://api.github.com/graphql",
    //       contentType: "application/json",
    //       headers: {
    //         Authorization: "bearer ***********"
    //       },
    //       data: JSON.stringify({
    //         query: `query ($entry: String!) {}`,
    //     variables: {
    //       "entry": $('#entry').val()
    //     }
    //   })
    // });
