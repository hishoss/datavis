# Generated by Django 2.0.2 on 2018-04-09 03:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0003_remove_chart_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='chart',
            name='column_y',
        ),
        migrations.AddField(
            model_name='chart',
            name='kpi_column_variables',
            field=models.CharField(default='', max_length=255, verbose_name='Column names for kpi variables'),
            preserve_default=False,
        ),
    ]
