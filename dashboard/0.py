
class ChartAugment:
    def __init__(self, chart_type, data, col_x, col_y):
        """
        This class sets the various defaults of different chart types
        :param chart_type: chartJS chart type
        :param data: blaze.data object
        """
        self.data = data
        self.chart_type = chart_type
        self.col_x = col_x
        self.col_y = col_y

    def augment(self):
        if self.chart_type in ['area', 'bar', 'doughnut', 'line', 'pie', 'polarArea', 'radar']:
            return self.gen_augment
        elif self.chart_type in ['bubble', 'scatter']:
            return self.scatter_augment
        else:
            raise AttributeError(self.chart_type + ' chart type not implemented yet')

    @property
    def gen_augment(self):
        # by(df.name, amount=df.amount.mean())
        # by(merge(df.name, df.id),amount=df.amount.mean())
        # labels, data1 = data.groupby(col_x, as_index=False).sum().fillna(0).values.transpose().tolist()

        # TODO: find a way to use other aggregations aside sum
        grouped_data = bz.by(self.data[self.col_x], aggregate=self.data[self.col_y].sum())
        labels = list(grouped_data[self.col_x])
        aggregate = list(grouped_data['aggregate'])
        kwargs = {'fill': False,
                  'backgroundColor': 'rgba(0, 250, 0, 0.75)',
                  'borderColor': 'rgba(0, 0, 0,1)',
                  'borderWidth': 0.8,
                  'hoverBackgroundColor': 'rgba(0, 250,0, 1)',
                  'hoverBorderColor': 'rgba(0, 0, 0, 0.75)'
                  }

        # this prevents having loads of legends on the chart
        display_legend = {'display': True} if self.data.count() >= 10 else {'display': False}
        return {'labels': labels, 'data': aggregate, 'kwargs': kwargs, 'display_legend': display_legend}

    @property
    def scatter_augment(self):
        """
        Augment dataframe into a list of dictionaries
        :return: [{'x': xval, 'y': yval}]
        """
        # TODO: find a better way to coerce types
        data = [{'x': x, 'y': y} for x, y in self.data][:5]
        kwargs = {'fill': False,
                  'borderWidth': 3}
        return {'labels': [], 'data': data, 'kwargs': kwargs}
        # return {'labels': None, 'data': data, 'kwargs': kwargs}
