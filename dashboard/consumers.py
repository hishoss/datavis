from random import randint, random, sample
import json

from channels.generic.websocket import JsonWebsocketConsumer, WebsocketConsumer

from .utils import augment_xy

'''
eg data
{'lineChart_data': sample(range(100), 10),
 'barChart_data': sample(range(100), 6),
 'comments': randint(0,100),
 'orders': randint(0, 15),
 'tasks': randint(0,10),
 'tickets': randint(0,50)}
'''


class DataStream(JsonWebsocketConsumer):
    def connect(self):
        if self.scope["user"].is_anonymous:
            self.close()
        else:
            self.accept()
            print("Connected to dashboard server!")

    def disconnect(self, close_code=None):
        # if close_code == ...
        self.close()
        print("Disconnected from dashboard server!")

    def receive_json(self, content, **kwargs):
        action = content['action']
        if action == 'close':
            self.disconnect()
        if action == 'more':
            self.send_json({'chart1_data': sample(range(100), 10),
                            'chart2_data': sample(range(100), 6),
                            'chart4_data': augment_xy(sample(range(100), 12), sample(range(100), 12)),
                            'comments': randint(0, 100),
                            'orders': randint(0, 15),
                            'tasks': randint(0, 10),
                            'tickets': randint(0, 50)})


class DataStream1(WebsocketConsumer):
    # data stream without json
    def connect(self):
        if self.scope["user"].is_anonymous:
            self.close()
        else:
            self.accept()
            print("Connected to dashboard server!")
            # self.send(text_data=json.dumps({'data': 'hello'}))
        
    def disconnect(self, close_code=None):
        # if close_code == ...
        self.close()
        print("Disconnected from dasboard server!")

    def receive(self, text_data=None, bytes_data=None):
        message = json.loads(text_data)
        action = message['action']
        if action == 'close':
            self.disconnect()
        if action == 'more':
            self.send(text_data=json.dumps({'chart1_data': sample(range(100), 10),
                                            'chart2_data': sample(range(100), 6),
                                            'chart4_data': augment_xy(sample(range(100), 12), sample(range(100), 12)),
                                            'comments': randint(0,100),
                                            'orders': randint(0, 15),
                                            'tasks': randint(0,10),
                                            'tickets': randint(0,50)}))


class DataStream00(WebsocketConsumer):
    def __init__(self, data):
        self.data = data
        super(DataStream00, self).__init__(self)

    def connect(self):
        self.accept()
        print("Server Connected!")
        # self.send(text_data=json.dumps({'data': 'hello'}))

    def disconnect(self, close_code=None):
        # if close_code == ...
        self.close()

    def receive(self, text_data=None, bytes_data=None):
        message = json.loads(text_data)
        action = message['action']
        if action == 'close':
            self.disconnect()
        if action == 'more':
            self.send(text_data=json.dumps(self.data))


# original. for realtime1
class DataStreamO(WebsocketConsumer):
    def connect(self):
        self.accept()
        print("Server Connected!")

    def disconnect(self, close_code):
        self.close()

    def receive(self, text_data=None, bytes_data=None):
        print("Client sent {}".format(text_data))
        data = [random() * 50 - 5, random() * 50 - 5, sample(range(100), 6)]
        self.send(text_data=json.dumps({"data": data}))
        # self.close()

        
class DataStream1(WebsocketConsumer):
    def connect(self):
        self.accept()
        print("Server Connected!")

    def disconnect(self, close_code):
        self.close()

    def receive(self, **kwargs):
        print("Client sent {}".format(kwargs['text_data']))
        data = sample(range(100), 20)
        self.send(text_data=json.dumps({"data": data}))          


class DataStream2(JsonWebsocketConsumer):
    def connect(self):
        print("Server Connected!")
        self.accept()

    def disconnect(self, close_code):
        self.close()

    def receive_json(self, content):
        # print("Client sent {}".format(content))
        data =  [random()*50 - 5, random()*50 - 5, sample(range(100), 6) ]
        self.send_json({"data": data})       
        # self.close()
