from jchart import Chart #, ChartScatter
from jchart.config import DataSet, rgba
from random import randint

from django.shortcuts import get_object_or_404
from .models import Chart as ChartM


def rand_color(n):
    return [rgba(randint(0, 220), randint(0, 220), randint(0, 220), 0.6)
            for _ in range(n)]


class Plot(Chart):
    responsive = True
    animation = {'duration': 2000}
    scales = {'yAxes': [{'ticks': {'beginAtZero': True},
                         'stacked': False}],
              'xAxes': [{'stacked': False}]}

    def __init__(self, pk):
        chart = get_object_or_404(ChartM, pk=pk)  # is it ok to use a get_object_or_404 in this object?
        self.chart_type = chart.type

        self.name = chart.name
        self.data = chart.aug['data']
        self.labels = chart.aug['labels']
        self.kwargs = chart.aug['kwargs']
        # self.legend = chart.aug.get('display_legend', {'display': True})
        self.legend = {'display': True} if len(self.data) <= 10 else {'display': False}

        super(Plot, self).__init__()

    def get_labels(self):
        return self.labels

    def get_datasets(self):
        # colors = rand_color(len(self.data))
        return [DataSet(label=self.name,
                        data=self.data,
                        # borderWidth=3,
                        # backgroundColor=colors,
                        # borderColor=colors,
                        **self.kwargs)]
