import graphene
from graphene_django.types import DjangoObjectType

from django.shortcuts import get_object_or_404

from .models import Chart


class ChartsType(DjangoObjectType):
    data = graphene.List(graphene.Float)
    labels = graphene.List(graphene.String)

    class Meta:
        model = Chart
        only_fields = ('name', 'type')


class Query:
    chart = graphene.Field(ChartsType, id=graphene.Int())
    all_charts = graphene.List(ChartsType)

    def resolve_chart(self, info, **kwargs):
        id = kwargs.get('id')
        if id:
            chart = get_object_or_404(Chart, id=id)
            if info.context.user.is_staff or info.context.user == chart.user:
                return chart
            else:
                return Chart.objects.none()
        return None

    def resolve_all_charts(self, info):
        if info.context.user.is_staff:
            return Chart.objects.all()
        if info.context.user.is_authenticated:
            return Chart.objects.filter(table__source_db__user=info.context.user)
        return Chart.objects.none()

    # def resolve_all_charts(self, info):
    #     return Chart.objects.all()
