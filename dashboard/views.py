from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect, render

from connection.models import Connection, Table

from .charts import Plot
from .forms import ChartAddForm
from .models import Chart


class DashboardView(LoginRequiredMixin, TemplateView):
    template_name = "dashboard/index.html"

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        context.update({'title': "Dashboard"})
        return context


class Dashboard2View(LoginRequiredMixin, TemplateView):
    template_name = "dashboard/dashboard.html"

    def get_context_data(self, **kwargs):
        context = super(Dashboard2View, self).get_context_data(**kwargs)
        context.update({'title': "Dashboard"})
        return context


class TimelineView(LoginRequiredMixin, TemplateView):
    template_name = "dashboard/timeline.html"

    def get_context_data(self, **kwargs):
        context = super(TimelineView, self).get_context_data(**kwargs)
        context.update({'title': "Timeline"})
        return context


@login_required
def chart_create(request, pk):
    table = get_object_or_404(Table, pk=pk)
    user = table.source_db.user
    if request.user == user or request.user.is_staff:

        form = ChartAddForm(pk, request.POST or None)
        context = {'con_pk': table.source_db_id,
                   'form': form,
                   'tb_pk': pk}
        if form.is_valid():
            chart = form.save(commit=False)
            # chart.user = user
            # chart.name = chart.column_x + ' vs ' + chart.column_y
            chart.table = table
            chart.save()
            messages.success(request, 'Chart created')
            return redirect(chart.table.source_db)
        return render(request, 'dashboard/chart_create.html', context)

    else:
        messages.warning(request, "Permission denied")
        redirect(request.user)


@login_required
def user_chart_list(request, username):
    if request.user.username == username or request.user.is_staff:
        charts = Chart.objects.filter(table__source_db__user__username=username)
        return render(request, 'dashboard/chart_list.html', {'charts': charts, 'username': username})
    else:
        messages.warning(request, "Permission denied")
        redirect(request.user)


@login_required
def connection_chart_list(request, con_pk):
    con = get_object_or_404(Connection, pk=con_pk)
    if request.user.username == con.user.username or request.user.is_staff:
        charts = Chart.objects.filter(table__source_db=con)
        return render(request, 'dashboard/chart_list.html', {'charts': charts, 'username': con.user.username})
    else:
        messages.warning(request, "Permission denied")
        redirect(request.user)


@login_required
def chart_detail(request, pk):
    chart = get_object_or_404(Chart, pk=pk)
    if request.user in chart.owner.users.all() or request.user.is_staff:
        updated = chart.updated
        username = chart.owner.name
        chart = Plot(pk)
        return render(request, 'dashboard/chart_detail.html',
                               {'chart': chart, 'updated': updated, 'username': username})
    else:
        messages.warning(request, "Permission denied")
        redirect(request.user)


@login_required
def chart_update(request, pk):
    chart = get_object_or_404(Chart, pk=pk)
    if request.user in chart.owner.users.all() or request.user.is_staff:

        tb_pk = chart.table_id
        con_pk = get_object_or_404(Table, pk=tb_pk).source_db_id
        form = ChartAddForm(tb_pk, request.POST or None, instance=chart)

        context = {"form": form,
                   "pk": pk,
                   "con_pk": con_pk}

        if form.is_valid():
            chart = form.save(commit=False)
            # chart.name = chart.column_x + ' vs ' + chart.column_y
            chart.save()
            messages.success(request, "Chart updated successfully")
            return redirect(chart)
        return render(request, "dashboard/chart_update.html", context)
    else:
        messages.warning(request, "Permission denied")
        redirect(request.user)


@login_required
def chart_delete(request, pk):
    chart = get_object_or_404(Chart, pk=pk)
    if request.user in chart.owner.users.all() or request.user.is_staff:
        chart.delete()
        messages.success(request, "Chart deleted")
        return redirect(chart.table.source_db)
    else:
        messages.warning(request, "Permission denied")
        redirect(request.user)


@login_required
def live_chart(request, pk):
    get_object_or_404(Chart, pk=pk)
    return render(request, 'dashboard/live_chart.html', {'pk': pk})





















# def dashing_dashboard(request, chart_pk):
#     chart_name = get_object_or_404(Chart, pk=chart_pk).name
#     widget = ChartWidget(chart_pk)
#     router.register(widget, chart_name)
#     return



# import requests
#
# url = 'https://api.github.com/graphql'
# json = { "query" : "{ viewer  { login } }" }
# api_token = "your api token here..."
# headers = {'Authorization': 'token %s' % api_token}
#
# r = requests.post(url=url, json=json, headers=headers)
# print (r.text)


# @login_required
# def charts(request, pk):
#     chart = get_object_or_404(Chart, pk=pk)
#     data = chart.table.data[[chart.column_x, chart.column_y]]
#     aug = ChartAugment(chart.type, data, chart.column_x, chart.column_y).augment()
#     print(aug['data'][:4])
#
#     return render(request, 'dashboard/charts/chart_detail1.html', {'data': json.dumps(aug['data'])})


# @login_required
# def plot(request):
#     cs = get_list_or_404(Chart, user=request.user)
#     data = {}
#     for c in cs:
#         df = c.table.data[[c.column_x, c.column_y]]
#         # we need two columns as arrays but this will make them rows and we can easily get separate arrays
#         x, y = df.values.transpose()
#
#         data[c.name] = augment_xy(x, y)
#     return render(request)


# @login_required
# def plot(request, pk):
#     context = {'pk': pk}
#     template = 'dashboard/charts.html'
#     return render(request, template, context)


# class ChartAjaxView(View):
#     def get(self, request, *args, **kwargs):
#         data = {}
#         if request.user.is_staff:
#             qs = '' # Order.objects.all().by_weeks_range(weeks_ago=5, number_of_weeks=5)
#             if request.GET.get('type') == 'chart-1':
#                 # basically trying to get the sales for each day but recompute total sales on each request
#                 days = 7
#                 start_date = timezone.now().today() - datetime.timedelta(days=days-1)
#                 datetime_list = []
#                 labels = []
#                 chart_items = []
#                 for x in range(0, days):
#                     new_time = start_date + datetime.timedelta(days=x)
#                     datetime_list.append(new_time)
#                     labels.append(new_time.strftime("%a"))
#                     new_qs = qs.filter(updated__day=new_time.day, updated__month=new_time.month)
#                     day_total = new_qs.totals_data()['total__sum'] or 0
#                     chart_items.append(day_total)
#
#                 data['labels'] = labels
#                 data['data'] = chart_items
#             if request.GET.get('type') == 'chart-2':
#                 data['labels'] = ["Four Weeks Ago", "Three Weeks Ago", "Two Weeks Ago", "Last Week", "This Week"]
#                 current = 5
#                 data['data'] = []
#                 for i in range(0, 5):
#                     new_qs = qs.by_weeks_range(weeks_ago=current, number_of_weeks=1)
#                     sales_total = new_qs.totals_data()['total__sum'] or 0
#                     data['data'].append(sales_total)
#                     current -= 1
#         return JsonResponse(data)


# class ChartView(LoginRequiredMixin, TemplateView):
#     template_name = 'dashboard/charts/live_chart.html'
#
#     def dispatch(self, *args, **kwargs):
#         user = self.request.user
#         if not user.is_staff:
#             return render(self.request, "400.html", {})
#         return super(ChartView, self).dispatch(*args, **kwargs)
#
#     def get_context_data(self, **kwargs):
#         context = super(ChartView, self).get_context_data(**kwargs)
#         qs = Order.objects.all().by_weeks_range(weeks_ago=10, number_of_weeks=10)
#         start_date = timezone.now().date() - datetime.timedelta(hours=24)
#         end_date = timezone.now().date() + datetime.timedelta(hours=12)
#         today_data = qs.by_range(start_date=start_date, end_date=end_date).get_sales_breakdown()
#         context['today'] = today_data
#         context['this_week'] = qs.by_weeks_range(weeks_ago=1, number_of_weeks=1).get_sales_breakdown()
#         context['last_four_weeks'] = qs.by_weeks_range(weeks_ago=5, number_of_weeks=4).get_sales_breakdown()
#         return context


# @login_required
# def kick(request):
#     exchange = {'2001-01-31': 1.064, '2002-01-31': 1.1305,
#                 '2003-01-31': 0.9417, '2004-01-31': 0.7937,
#                 '2005-01-31': 0.7609, '2006-01-31': 0.827,
#                 '2007-01-31': 0.7692, '2008-01-31': 0.6801,
#                 '2009-01-31': 0.7491, '2010-01-31': 0.7002,
#                 '2011-01-31': 0.7489, '2012-01-31': 0.7755,
#                 '2013-01-31': 0.7531,
#                 }
#
#     browser_stats = [['Chrome', 52.9], ['Firefox', 27.7], ['Opera', 1.6],
#                      ['Internet Explorer', 12.6], ['Safari', 4]]
#
#     temperature = [{u'data': {'2012-00-01 00:00:00 -0700': 7,
#                               '2012-01-01 00:00:00 -0700': 6.9,
#                               '2012-02-01 00:00:00 -0700': 9.5,
#                               '2012-03-01 00:00:00 -0700': 14.5,
#                               '2012-04-01 00:00:00 -0700': 18.2,
#                               '2012-05-01 00:00:00 -0700': 21.5,
#                               '2012-06-01 00:00:00 -0700': 25.2,
#                               '2012-07-01 00:00:00 -0700': 26.5,
#                               '2012-08-01 00:00:00 -0700': 23.3,
#                               '2012-09-01 00:00:00 -0700': 18.3,
#                               '2012-10-01 00:00:00 -0700': 13.9,
#                               '2012-11-01 00:00:00 -0700': 9.6},
#                     u'name': u'Tokyo'},
#                    {u'data': {'2012-00-01 00:00:00 -0700': -0.2,
#                               '2012-01-01 00:00:00 -0700': 0.8,
#                               '2012-02-01 00:00:00 -0700': 5.7,
#                               '2012-03-01 00:00:00 -0700': 11.3,
#                               '2012-04-01 00:00:00 -0700': 17,
#                               '2012-05-01 00:00:00 -0700': 22,
#                               '2012-06-01 00:00:00 -0700': 24.8,
#                               '2012-07-01 00:00:00 -0700': 24.1,
#                               '2012-08-01 00:00:00 -0700': 20.1,
#                               '2012-09-01 00:00:00 -0700': 14.1,
#                               '2012-10-01 00:00:00 -0700': 8.6,
#                               '2012-11-01 00:00:00 -0700': 2.5},
#                     u'name': u'New York'},
#                    {u'data': {'2012-00-01 00:00:00 -0700': -0.9,
#                               '2012-01-01 00:00:00 -0700': 0.6,
#                               '2012-02-01 00:00:00 -0700': 3.5,
#                               '2012-03-01 00:00:00 -0700': 8.4,
#                               '2012-04-01 00:00:00 -0700': 13.5,
#                               '2012-05-01 00:00:00 -0700': 17,
#                               '2012-06-01 00:00:00 -0700': 18.6,
#                               '2012-07-01 00:00:00 -0700': 17.9,
#                               '2012-08-01 00:00:00 -0700': 14.3,
#                               '2012-09-01 00:00:00 -0700': 9,
#                               '2012-10-01 00:00:00 -0700': 3.9,
#                               '2012-11-01 00:00:00 -0700': 1},
#                     u'name': u'Berlin'},
#                    {u'data': {'2012-00-01 00:00:00 -0700': 3.9,
#                               '2012-01-01 00:00:00 -0700': 4.2,
#                               '2012-02-01 00:00:00 -0700': 5.7,
#                               '2012-03-01 00:00:00 -0700': 8.5,
#                               '2012-04-01 00:00:00 -0700': 11.9,
#                               '2012-05-01 00:00:00 -0700': 15.2,
#                               '2012-06-01 00:00:00 -0700': 17,
#                               '2012-07-01 00:00:00 -0700': 16.6,
#                               '2012-08-01 00:00:00 -0700': 14.2,
#                               '2012-09-01 00:00:00 -0700': 10.3,
#                               '2012-10-01 00:00:00 -0700': 6.6,
#                               '2012-11-01 00:00:00 -0700': 4.8},
#                     u'name': u'Լոնդոն'}]
#
#     sizes = [['X-Small', 5], ['Small', 27], ['Medium', 10],
#              ['Large', 14], ['X-Large', 10]]
#
#     areas = {'2013-07-27 07:08:00 UTC': 4, '2013-07-27 07:09:00 UTC': 3,
#              '2013-07-27 07:10:00 UTC': 2, '2013-07-27 07:04:00 UTC': 2,
#              '2013-07-27 07:02:00 UTC': 3, '2013-07-27 07:00:00 UTC': 2,
#              '2013-07-27 07:06:00 UTC': 1, '2013-07-27 07:01:00 UTC': 5,
#              '2013-07-27 07:05:00 UTC': 5, '2013-07-27 07:03:00 UTC': 3,
#              '2013-07-27 07:07:00 UTC': 3}
#     # There seems to be an object of form <WSGIRequest: GET '/kick/'>  as a last value of locals().
#     # Will this always be the last value?
#     return render(request, 'components/kick.html', {'data': list(locals().values())[:-1]})
