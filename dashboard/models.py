from ast import literal_eval
from blaze import transform

from django.db import models
from django.shortcuts import reverse

from connection.models import Table
from metric.models import KPI
from users.models import Organisation, User

from .utils import ChartAugment

# TODO: this app should eventually be called dashboard

TYPE_CHOICES = (
    ('area', 'Area'),
    ('bar', 'Bar'),
    ('bubble', 'Bubble'),
    ('doughnut', 'Doughnut'),
    ('line', 'Line'),
    ('pie', 'Pie'),
    ('polarArea', 'Polar Area'),
    ('scatter', 'Scatter'),
    ('radar', 'Radar')
)
FREQUENCY_CHOICES = (
    ('hourly', 'Hourly'),
    ('daily', 'Daily'),
    ('weekly', 'Weekly'),
    ('monthly', 'Monthly'),
    ('quarterly', 'Quarterly'),
    ('annually', 'Annually'),
)
UNIT_CHOICES = (
    ('raw', 'Raw value'),
    ('percent', 'Percentage'),
)


class Chart(models.Model):
    owner = models.ForeignKey(Organisation, on_delete=models.CASCADE)
    # name = models.CharField(max_length=255)
    type = models.CharField(max_length=9, choices=TYPE_CHOICES, default='area')
    kpi = models.ForeignKey(KPI, on_delete=models.CASCADE)
    objective = models.CharField(max_length=255, help_text='What does the chosen kpi intend to achieve?')
    target_value = models.FloatField(help_text="Target value of chosen kpi.")
    unit = models.CharField(max_length=10, choices=UNIT_CHOICES)
    # TODO: is this the same thing as what we have in the package? i.e how frequent to update the chart
    frequency = models.CharField(max_length=10, choices=FREQUENCY_CHOICES)
    start_date = models.DateField()
    end_date = models.DateField(blank=True, null=True)
    table = models.ForeignKey(Table, on_delete=models.CASCADE)
    # TODO: this may be time, etc
    column_x = models.CharField(max_length=255, verbose_name='Column for x-axis')
    kpi_column_variables = models.CharField(max_length=255, verbose_name='Dictionary of kpi variables and column names')
    timestamp = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('updated',)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('dashboard:detail', kwargs={'pk': self.pk})

    # @property
    # def user(self):
    #     return self.table.source_db.user

    @property
    def variable_mappings(self):
        """
        Map data source columns to kpi variables
        :return:
        """
        return self.kpi.variable_mappings

    def compute(self, data):
        """
        Map data source columns to kpi variables
        :param data: blaze.data object
        :return: computed kpi
        """
        variables = {k: data[v] for k, v in literal_eval(self.kpi_column_variables).items()}
        return self.kpi.compute(variables)

    @property
    def aug(self):
        data = self.table.data

        # TODO: Can we do this out of core?
        # Create new blaze.data object.
        data = transform(data, computed_kpi=self.compute(data))

        # TODO: this has the potential to load everything
        return ChartAugment(self.type, data, self.column_x).augment()

    @property
    def data(self):
        return self.aug['data']

    @property
    def labels(self):
        return self.aug['labels']

    @property
    def name(self):
        return self.kpi.name


class Dashboard(models.Model):
    name = models.CharField(max_length=255)
    charts = models.ManyToManyField(Chart)
    description = models.TextField()
    updated = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('updated',)

    def __str__(self):
        return self.name
