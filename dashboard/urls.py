from django.urls import path

from . import views

app_name = 'dashboard'


urlpatterns = [
    path('', views.DashboardView.as_view(), name="home"),
    path('<int:pk>/create', views.chart_create, name='create'),
    path('<int:pk>/delete', views.chart_delete, name='delete'),
    path('<int:pk>/detail', views.chart_detail, name='detail'),
    path('<int:pk>/live', views.live_chart, name='live'),
    path('<int:pk>/update', views.chart_update, name='update'),
    path('<int:con_pk>/list', views.connection_chart_list, name='connection_chart_list'),
    path('<str:username>/list', views.user_chart_list, name='user_chart_list'),
    path('dash', views.Dashboard2View.as_view(), name="dash"),
    path('timeline/', views.TimelineView.as_view(), name="timeline"),
]
