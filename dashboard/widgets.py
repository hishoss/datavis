# from collections import Counter
# from numpy import NaN
# from dashing.widgets import NumberWidget, Widget, GraphWidget, ListWidget
# from datetime import datetime, timedelta
# from random import randint
#
# from django.shortcuts import get_object_or_404
# from chart.models import Chart
#
# from connection.models import Connection
#
#
# users = randint(50, 100)
# counts = 0
#
#
# class NewClientsWidget(NumberWidget):
#     title = 'OSSTech Automobile Horsepower'
#     data = []  # list(Connection.objects.get(user__username='hisham', name='automobile').data()['horsepower'])
#     # counts = len(data)
#
#     def get_value(self):
#         global counts
#         counts += 1
#         return counts
#
#     def get_detail(self):
#         global users
#         return '{} actives'.format(int(users/3))
#
#     def get_more_info(self):
#         global users
#         return '{} fakes'.format(int(users/10))
#
#     def get_data(self):
#         return self.data
#
#
# class ValueWidget(NumberWidget):
#     # title = 'New Users'
#
#     def get_value(self):
#         return users
#
#     def get_detail(self):
#         global users
#         return '{} actives'.format(int(users/3))
#
#     def get_more_info(self):
#         global users
#         return '{} fakes'.format(int(users/10))
#
#
# class OSSTechWidget(GraphWidget):
#     title = 'OSSTech Graph'
#     more_info = 'lalala'
#     data = []
#
#     def get_value(self):
#         return 111
#
#     def get_data(self):
#
#         return [{'x': x, 'y': x} for x in range(5)]
#                 # for x, y in zip(range(len(self.data)), map(lambda x: x + randint(0, 21), self.data))]
#
#     def get_updated_at(self):
#         return str(datetime.now())
#
#
# # class ChartWidget(GraphWidget):
# #     def __init__(self, pk):
# #         chart = get_object_or_404(Chart, pk=pk)
# #         self.title = chart.name
# #         self.more_info = ''
# #         self.data = chart.data
# #         self.labels = chart.labels
# #         self.updated = chart.updated
# #         super(ChartWidget, self).__init__()
# #
# #     def get_value(self):
# #         return 111
# #
# #     def get_data(self):
# #         return [{'x': x, 'y': y} for x, y in zip(self.labels, self.data)]
# #
# #     def get_updated_at(self):
# #         return self.updated
#
#
# class ChartWidget(GraphWidget):
#     def __init__(self):
#         chart = get_object_or_404(Chart, pk=1)
#         self.title = chart.name
#         self.more_info = ''
#         self.data = chart.data
#         self.labels = chart.labels
#         self.updated = chart.updated
#         self.type = chart.type
#         super(ChartWidget, self).__init__()
#
#     def get_data(self):
#         # return [{'x': x, 'y': int(y)} for x, y in zip(self.labels, self.data)]
#         return [{'x': x, 'y': x + randint(0, 100)} for x in range(20)]
#
#     def get_updated_at(self):
#         return self.updated
#
#     def get_context(self):
#         return {
#             'title': self.get_title(),
#             'moreInfo': self.get_more_info(),
#             'value': self.get_value(),
#             'data': self.get_data(),
#             'properties': self.get_properties(),
#             'type': self.type,
#             'labels': self.labels,
#         }
#
#
# class ItemListWidget(ListWidget):
#     title = 'Items'
#     more_info = 'blah blah'
#     data = []
#
#    # def get_updated_at(self):
#    #     modified = SearchQuerySet().filter(
#    #         django_ct='errand').order_by('-modified')[0].modified
#    #     return u'Last updated {}'.format(modified)
#
#     def get_data(self):
#         counts = Counter(self.data)
#         return [{'label':x, 'value':y} for x, y in counts]
#
