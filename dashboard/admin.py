from django.contrib import admin
from .models import Chart, Dashboard


class ChartAdmin(admin.ModelAdmin):
    list_display = ('owner', 'name', 'type', 'kpi', 'frequency', 'start_date', 'updated')  # + user
    list_filter = ('owner', 'start_date', 'updated', 'timestamp')  # + user
    # prepopulated_fields = {'name': ('column_x', 'column_y')}


class DashboardAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')
    list_filter = ('updated', 'name')


admin.site.register(Chart, ChartAdmin)
admin.site.register(Dashboard, DashboardAdmin)

