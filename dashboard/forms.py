from django import forms
from connection.models import Table

from .models import Chart


class ChartAddForm(forms.ModelForm):
    class Meta:
        model = Chart
        fields = ('type', 'column_x', 'kpi_column_variables')

    def __init__(self, pk, *args, **kwargs):
        super(ChartAddForm, self).__init__(*args, **kwargs)
        columns = Table.objects.get(pk=pk).columns
        columns = forms.Select(choices=zip(columns, columns))
        self.fields['column_x'].widget = columns
        # self.fields['column_y'].widget = columns


