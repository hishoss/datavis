import blaze as bz
from odo import odo

from subprocess import Popen

from django.contrib import messages
from django.contrib.auth.decorators import login_required
# from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.text import slugify
# from graphene_django.views import GraphQLView

from .app_settings import MEDIA_URL, MEDIA_ROOT, RAW_DB_URL
from .forms import AvailableTablesForm, connection_form, LoadedTablesForm, SQLForm
from .models import Connection, Table
from .tasks import create_periodic_task, commit_table
from .terms_and_conditions import connection_terms
from .utils import create_notebook, delete_table, delete_tables, make_db_table_name, tables_loaded


# confluent start kafka

@login_required
def connection_create(request, type):
    form = connection_form(type)(request.POST or None, request.FILES)
    if form.is_valid():
        con = form.save(commit=False)
        con.user = request.user
        if type == 'remote':
            con.type = 'rfile'
        if con.connection_status == 1:
            con.is_valid = True
        con.save()
        messages.success(request, 'Database connection was successfully added')
        return redirect(con)
    else:
        form = connection_form(type)
    return render(request, "connection/create.html", {'form': form, 'type': type, 'terms': connection_terms})


@login_required
def connection_update(request, pk):
    con = get_object_or_404(Connection, pk=pk)
    # the types we use in the create view are different. if it's none of csv, excel and rfile
    # then we assume it must be one of the external db types
    type = {'csv': 'flatfile', 'excel': 'flatfile', 'rfile': 'remote'}.get(con.type, 'externaldb')

    form = connection_form(type)(request.POST or None, request.FILES, instance=con)
    if form.is_valid():
        con = form.save(commit=False)
        if con.connection_status:
            con.is_valid = True
            msg = "Database connection was successfully updated"
        else:
            con.is_valid = False
            msg = "Database connection updated but a connection couldn't be established"
        con.save()
        messages.success(request, msg)
        return redirect(con)
    else:
        form = connection_form(type)(instance=con)
    return render(request, "connection/update.html", {"form": form, "con": con})


@login_required
def connection_delete(request, pk):
    con = Connection.objects.get(pk=pk)
    if request.user == con.user or request.user.is_staff:
        delete_tables(pk)
        con.delete()
        messages.success(request, "Database Connection removed")
        return redirect('connection:list')
    else:
        messages.warning(request, 'Access denied')
        return redirect(request.user)


@login_required
def connection_detail(request, pk):
    con = get_object_or_404(Connection, pk=pk)

    if request.user == con.user or request.user.is_staff:
        loaded_tbs = tables_loaded(con).keys()
        available_tbs = sorted(set(con.table_names) - loaded_tbs)

        context = {'con': con, 'sqlform': SQLForm(),
                   'loaded_tables_form': LoadedTablesForm(sorted(loaded_tbs)),
                   'unloaded_tables_form': AvailableTablesForm(available_tbs)}
        return render(request, 'connection/detail.html', context)
    else:
        messages.warning(request, 'Access denied')
        return redirect(request.user)


@login_required
def loaded_tables(request, pk):
    # TODO: could we make this view just a function
    con = get_object_or_404(Connection, pk=pk)
    loaded_tbs = tables_loaded(con)

    form = LoadedTablesForm(sorted(loaded_tbs.keys()), request.POST or None)
    if form.is_valid():
        table = form.cleaned_data['table']
        table_pk = loaded_tbs[table]
        if 'details' in request.POST:
            return redirect('connection:table_detail', table_pk)

        elif 'remove' in request.POST:
            return redirect('connection:table_delete', table_pk)

        elif 'clear' in request.POST:
            return redirect('connection:tables_clear', pk)

        elif 'load+' in request.POST:
            table_name = form.cleaned_data['table']
            name = make_db_table_name(con, table_name)
            tb = Table.objects.get(source_db=con, name=name)

            if tb.state == 'cleaned':
                create_periodic_task(tb.pk, name)
                messages.info(request, f"Consuming new data from {table_name}.")
            else:
                messages.warning(request, "Cannot send data to warehouse at the moment. Data from this connection yet "
                                          "to be cleaned")
            return redirect(con)

        elif 'chart_create' in request.POST:
            return redirect('chart:create', table_pk)

        elif 'analyse' in request.POST:
            from .utils import extract_jupyter_url

            # Notebook server closes after 5 minutes of inactivity
            Popen(['nohup', 'jupyter-notebook', MEDIA_ROOT + con.user.username + '/notebooks/'],
            # Popen(['nohup', 'jupyter-notebook', 'user_notebooks/' + con.user.username],
                  stdout=open('/dev/null', 'w'),
                  stderr=open('jupyter_stderr.log', 'a'))

            # TODO: we need to terminate the notebooks after completion
            # pid = p.id  # we can use this to terminate it from other locations
            # p.terminate()
            # TODO: if so many users are trying to analyse tables, the url might not point to the correct one
            url = extract_jupyter_url('jupyter_stderr.log')
            messages.info(request, f"Navigate to '{url}' and use the password provided by your administrator")
            return redirect(con)
    else:
        return redirect('connection:detail', pk)


@login_required
def table_add(request, pk):
    con = get_object_or_404(Connection, pk=pk)
    loaded_tbs = tables_loaded(con)
    available_tbs = sorted(set(con.table_names) - loaded_tbs.keys())

    form = AvailableTablesForm(available_tbs, request.POST or None)
    if form.is_valid():
        table_name = form.cleaned_data['table']
        pk_column = form.cleaned_data.get('primary_key_column') or 'id'

        # TODO: a signal could be used for this in the model, so that, we just pass in the raw name to the table model
        name = make_db_table_name(con, table_name)

        tb = Table.objects.create(source_db=con, name=name, primary_key_column=pk_column)
        create_notebook(con.user.username, name)
        commit_table.delay(tb.pk, RAW_DB_URL)
        messages.info(request, f"Loading from {table_name}!")
        return redirect(con)
    else:
        return redirect('connection:detail', pk)


@login_required
def tables_clear(request, pk):
    """Clear loaded tables from connection
    """
    con = Connection.objects.filter(pk=pk).first()

    if (con.user == request.user) or request.user.is_staff:
        delete_tables(pk)
        messages.success(request, 'Tables cleared')
        return redirect(con)

    else:
        messages.warning(request, 'Access denied')
        return redirect(request.user)


@login_required
def table_delete(request, pk):
    tb = get_object_or_404(Table, pk=pk)
    if (tb.source_db.user == request.user) or request.user.is_staff:
        delete_table(tb)
        messages.success(request, f'{tb.just_name} removed')
        con = get_object_or_404(Connection, pk=tb.source_db_id)
        return redirect(con)
    else:
        messages.warning(request, 'Access denied')
        return redirect(request.user)


@login_required
def table_detail(request, pk):
    tb = get_object_or_404(Table, pk=pk)
    if (tb.source_db.user == request.user) or request.user.is_staff:
        table_html = tb.to_html
        prefix = tb.source_db.user.username + '_' + tb.source_db.name + '__'
        name = tb.name.replace(prefix, '')
        template = 'connection/table_detail.html'
        return render(request, template, {'name': name, 'tb': tb, 'table_html': table_html})
    else:
        messages.warning(request, 'Access denied')
        return redirect(request.user)


# @login_required
# def table_update(request, pk):
#     tb = get_object_or_404(Table, pk)
#     form = TableIdFieldForm(tb.columns, request.POST or None)
#     if form.is_valid():
#         tb.primary_key_column = form.cleaned_data['primary_key_column']
#         tb.save()
#         return redirect(tb.source_db)
#     else:
#         return render(request, '', {'form': TableIdFieldForm(tb.columns)})


@login_required
def table_sql_add(request, pk):
    # TODO: this field is very experimental
    con = get_object_or_404(Connection, pk=pk)
    form = SQLForm(request.POST or None)
    if form.is_valid():
        # TODO: we need some serious pre-processing of this sql statement to avoid anything malicious
        # TODO: .get('') or something
        query = form.cleaned_data.get('query')
        pk_column = form.cleaned_data.get('primary_key_column')
        table_name = slugify(form.cleaned_data['name']).replace('-', '_')
        name = make_db_table_name(con, table_name)

        tb = Table.objects.create(source_db=con, name=name, primary_key_column=pk_column, query=query)
        create_notebook(con.user.username, name)
        commit_table.delay(tb.pk, RAW_DB_URL)
        messages.info(request, f"Loading from {table_name}!")

        return redirect(con)
    else:
        return redirect('connection:detail', pk)
