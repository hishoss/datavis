from django import forms
from django.forms.models import modelform_factory
from django.forms.widgets import PasswordInput

from .models import Connection, Table


def connection_form(type):
    if type == 'externaldb':
        fields = ("type", "username", "password", "port", "hostname", "name")
    elif type == 'flatfile':
        fields = ("type", "name", "path")
    elif type == 'remote':
        fields = ("username", "password", "port", "hostname", "name")
    else:
        raise Exception("type not correct")

    return modelform_factory(model=Connection, fields=fields, widgets={"password": PasswordInput})


class LoadedTablesForm(forms.Form):
    table = forms.ChoiceField(choices=(), required=True)

    def __init__(self, tables, *args, **kwargs):
        super(LoadedTablesForm, self).__init__(*args, **kwargs)
        self.fields['table'].choices = list(zip(tables, tables))


class AvailableTablesForm(LoadedTablesForm):
    primary_key_column = forms.CharField(max_length=255, required=False)
    add_id = forms.BooleanField(label="Has no id field", required=False)


class SQLForm(forms.ModelForm):
    class Meta:
        model = Table
        fields = ('name', 'primary_key_column', 'query')
        widgets = {'query': forms.Textarea}


class ConnectionForm(forms.ModelForm):
    class Meta:
        model = Connection
        exclude = ('password',)
