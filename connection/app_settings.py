from django.conf import settings


# KAFKA_HOST_URL = getattr(settings, 'KAFKA_HOST_URL', 'localhost:9092')
# WAREHOUSE_DB_URL = getattr(settings, 'WAREHOUSE_DB_URL', 'sqlite:///db_store/warehouse.db')
WAREHOUSE_DB_URL = getattr(settings, 'WAREHOUSE_DB_URL', None)
RAW_DB_URL = getattr(settings, 'RAW_DB_URL', None)
BASE_DIR = getattr(settings, 'BASE_DIR', None)
MEDIA_URL = getattr(settings, 'MEDIA_URL', None)
MEDIA_ROOT = getattr(settings, 'MEDIA_ROOT', None)
