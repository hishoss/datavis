from django.contrib import admin

from .forms import ConnectionForm
from .models import Connection, Table


class ConnectionAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'user', 'timestamp')
    list_filter = ('type', 'timestamp')
    form = ConnectionForm


class TableAdmin(admin.ModelAdmin):
    list_display = ('name', 'source_db', 'state', 'timestamp')
    list_filter = ('timestamp',)


admin.site.register(Connection, ConnectionAdmin)
admin.site.register(Table, TableAdmin)
