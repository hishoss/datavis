from confluent_kafka import Consumer, KafkaError, Producer
from numpy import loads
from sqlalchemy import create_engine, Column, MetaData, Table
from sqlalchemy.types import Boolean, DateTime, Float, Integer, String
from time import time

from .app_settings import KAFKA_HOST_URL


def create_row(columns, msg):
    """Return a dictionary of {column: data} representing a single row in connection
       columns: column names in connection
       msg: a row from connection without column names
    """
    return {col: msg[i] for i, col in enumerate(columns)}

# def


def kafka_settings(username, host=KAFKA_HOST_URL):
    config = {
        'bootstrap.servers': host,
        'group.id': username + '-group',
        'client.id': username,
        'enable.auto.commit': True,
        'session.timeout.ms': 6000,
        # 'security.protocol': 'ssl',
        # 'ssl.ca.location': 'CARoot.pem',
        # 'ssl.certificate.location': 'certificate.pem',
        # 'ssl.key.location': 'key.pem',
        'compression.codec': 'snappy',
        'default.topic.config': {'auto.offset.reset': 'smallest'}
    }
    return config


def make_table(db_url, table_name, columns, dtypes, primary_key_column):
    assert primary_key_column in columns

    engine = create_engine(db_url)
    # TODO: if table doesn't exist create one
    post_meta = MetaData(bind=engine)
    connection = engine.connect()

    # convert dtypes into sqlalchemy dtypes
    dt = {'bool': Boolean, 'datetime64[ns]': DateTime, 'datetime64[ns, UTC]': DateTime, 'float64': Float,
          'int64': Integer, 'object': String, 'string': String}

    # TODO: For now, an id field is added to the table. Table coming in may have its own id field. That's for later
    sq_dtypes = [dt[str(d)] for d in dtypes]

    # TODO: It's assumed the data comes from connection or source which has an id field.
    cols = []
    for col, dtype in zip(columns, sq_dtypes):
        if col == primary_key_column:
            cols.append(Column(col, Integer, primary_key=True))
        else:
            cols.append(Column(col, dtype))
    table = Table(table_name, post_meta, *cols)
    return table, post_meta, connection


def transfer_db(columns, dtypes, topic, username, table_name, destination_db_url, primary_key_column, verbose=True):
    table, post_meta, connection = make_table(destination_db_url, table_name, columns, dtypes, primary_key_column)
    table.create(checkfirst=True)
    post_meta.reflect(only=[table_name])
    table = post_meta.tables[table_name]
    rows = []
    connection.execute(table.insert(), rows)


#

def consume(columns, dtypes, topic, username, table_name, destination_db_url, primary_key_column, verbose=True):
    config = kafka_settings(username)
    c = Consumer(**config)
    c.subscribe([topic])

    table, post_meta, connection = make_table(destination_db_url, table_name, columns, dtypes, primary_key_column)
    table.create(checkfirst=True)
    post_meta.reflect(only=[table_name])
    table = post_meta.tables[table_name]  # do we need this
    last_commit = int(time())
    timeout = 6
    rows = []

    try:
        while True:
            if (int(time()) - last_commit) > timeout:
                print(f'Done consuming data from {topic}.\n No more messages')
                break

            msg = c.poll(10)
            if msg is None:
                continue
            elif not msg.error():
                # write to some log instead of just printing
                # TODO: let's just receive bulk messages
                data = loads(msg.value())
                if verbose:
                    print("Received message")
                # upon receiving a row of data, which is an arrary,
                # deserialize, create instance, and then append to rows list
                row = []
                rows.append(create_row(columns, row))
            elif msg.error().code() == KafkaError._PARTITION_EOF:
                # when we get to the end of data stream, we insert data then clear the
                connection.execute(table.insert(), rows)
                rows = []
                print(f'End of partition reached topic: {msg.topic()} partition: {table_name}.\n Session committed')
                last_commit = time()
                continue
            else:
                print("Error occurred: {0}".format(msg.error().str()))
                return f"Error occurred {msg.error().str()}"
    except KeyboardInterrupt:
        return "Consume: Interrupted"
    connection.close()
    c.close()
    return "Consume: Done"


def pprint(err, msg):
    """Basic callback to format messages being sent
    """
    if err is not None:
        print(f"Failed to deliver message: {msg.value()}: {err.str()}")
    else:
        print(f"Message produced: {msg.value()}")


def produce(df, topic, username, callback=pprint):
    """Send the document containing rows of data to the consumer
       topic: name of topic consumer is listening to
       document: file containing data. Only csv and excel is
       supported at this moment
       we may need to pass in an extra argument to handle other dbtype
       callback: ...
    """
    config = kafka_settings(username)
    p = Producer(**config)

    data = df.values

    try:
        p.produce(topic, data.dumps(), callback=callback)
        p.poll(0.5)
    except KeyboardInterrupt:
        return "Produce: Interrupted"

    p.flush()


    # try:
    #     # TODO: either send entire data or send in chunks. Like divide data into 10 parts
    #     for row in data:
    #         # .dumps() creates a serializable version of the list
    #         p.produce(topic, row.dumps(), callback=callback)
    #         p.poll(0.5)
    # except KeyboardInterrupt:
    #     return "Produce: Interrupted"

    p.flush()

    return "Produce: Done"
