# import dask.dataframe as dd
import blaze as bz
from blaze import discover, resource
from odo import odo
import pandas as pd
import sqlalchemy as sq
from sqlalchemy.engine import url as sq_url
from sqlalchemy.exc import ArgumentError, OperationalError

from django.db import models
from django.shortcuts import reverse
from users.models import User

from .app_settings import RAW_DB_URL, WAREHOUSE_DB_URL
from .utils import db_upload_path
from .validators import full_domain_validator, port_regex


class Connection(models.Model):
    TYPE_CHOICES = (
        ('redshift', 'Amazon Redshift'),
        ('csv', 'CSV'),
        ('excel', 'Excel'),
        ('mysql', 'MySQL'),
        ('mssql', 'Microsoft SQL Server'),
        ('oracle', 'Oracle'),
        ('postgresql', 'PostgreSQL'),
        ('rfile', 'Remote File'),
        ('sqlite', 'SQLite'),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    type = models.CharField(max_length=10, choices=TYPE_CHOICES)
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=100, blank=True, default='')
    hostname = models.CharField(max_length=100, validators=[full_domain_validator])
    port = models.CharField(max_length=5, validators=[port_regex])
    name = models.CharField(max_length=20)
    path = models.FileField(upload_to=db_upload_path, blank=True, verbose_name="If connection is available locally")
    is_valid = models.BooleanField(default=False)
    updated = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'Connections'

    def __str__(self):
        return f"{self.name}"

    @property
    def connection_uri(self):
        """
        Returns a connection uri to the data source
        :return: sqlAlchemy connection uri
        """
        if self.type in ['csv', 'excel']:
            return self.path.url

        if self.type == 'sqlite':
            return "sqlite:///" + self.path.url

        elif self.type == 'rfile':
            # TODO: get a server, place a file on it and find a way to connect to this
            return self.path.url

        elif self.type != 'csv' and self.type != 'excel' and self.type != 'sqlite' and self.type != 'rfile':
            db_connect_url = sq_url.URL(
                drivername=self.type,
                username=self.username,
                password=self.password,
                host=self.hostname,  # TODO: if user entered protocol eg http://host.com, shit happens
                port=self.port or 0,
                database=self.name)
            return db_connect_url

    @property
    def connection(self):
        if self.type not in ['sqlite']:
            return sq.create_engine(self.connection_uri, connect_args={'connect_timeout': 10})
        else:
            return sq.create_engine(self.connection_uri)

    @property
    def connection_status(self):
        # TODO: remote file
        try:
            self.connection.connect()
            self.connection.dispose()
            return True
        # This error occurs on flat file dbs
        except (ArgumentError, AttributeError):
            return True
        except OperationalError:
            # occurs if connection is wrong or unreachable
            return False

    @property
    def table_names(self):
        if self.type in ['csv', 'excel', 'rfile']:
            return [self.name]
        if not self.connection_status:
            # this, when connection is invalid
            return []
        return self.connection.table_names()

    def get_absolute_url(self):
        return reverse('connection:detail', kwargs={'pk': self.pk})


class Table(models.Model):
    CHOICES = (
        ('raw', 'Raw'),
        ('cleaned', 'Cleaned')
    )
    source_db = models.ForeignKey(Connection, on_delete=models.CASCADE, related_name='tables')
    name = models.CharField(max_length=255, unique=True)
    # TODO: we need to ensure this field will be unique ints
    primary_key_column = models.CharField(max_length=255, default='id')
    state = models.CharField(max_length=10, choices=CHOICES, default='raw')
    query = models.CharField('SQL SELECT Statement', max_length=1000, null=True)
    last_committed_id = models.PositiveIntegerField(default=-1)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        pass

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('connection:table_detail', kwargs={'pk': self.pk})

    @property
    def columns(self):
        try:
            return self.data.fields
        except (AttributeError, ValueError):
            return []

    def column(self, column_name):
        if column_name not in self.columns:
            return []
        else:
            return self.data[column_name].tolist()

    @property
    def connection(self):
        """
        Creates a connection to database containing table
        :return: an sqlalchemy engine object
        """
        if self.state == 'raw':
            return sq.create_engine(RAW_DB_URL)
        if self.state == 'cleaned':
            return sq.create_engine(WAREHOUSE_DB_URL)

    @property
    def connection0(self):
        """
        Creates a reverse connection to database containing table
        :return: an sqlalchemy engine object
        """
        if self.state == 'raw':
            return sq.create_engine(WAREHOUSE_DB_URL)
        if self.state == 'cleaned':
            return sq.create_engine(RAW_DB_URL)

    @property
    def just_name(self):
        return self.name.split('__')[-1]

    @property
    def __table_uri(self):
        uri = str(self.source_db.connection_uri)
        if self.source_db.type not in ['csv', 'excel']:
            uri += '::' + self.just_name

        return resource(uri, primary_key=[self.primary_key_column])

    @property
    def __dshape(self):
        # TODO: make id field unique. Should we store this field?

        pkc = self.primary_key_column
        dshape = str(discover(self.__table_uri))
        dshape_lst = dshape.splitlines()
        # basically replace 'map[int...' with int32
        # TODO: are there ever 'map[int64...'?
        dshape = [(x.split(':')[0] + ': int64').rstrip(',') if 'map[int' in x else x.rstrip(',') for x in dshape_lst]
        dshape = ','.join(dshape).replace('{,', '{')

        # if source_db is a csv/excel it won't come with a proper schema enforcing constraints,
        #  so we'll let all columns apart from id be nullable
        if self.source_db.type in ['csv', 'excel']:
            # TODO: use a regex for this.
            dshape = dshape.replace(': ?', ': ')  # remove all '?' else we will end with some doubles
            dshape = dshape.replace(': ', ': ?')  # add '?' infront of all types
            dshape = dshape.replace(pkc + ': ?', pkc + ': ')  # remove the '?' infront of id's type

        # TODO: Due to pandas/blaze not supporting NAN/null values in int columns, replace them by floats
        return dshape.replace('int', 'float').replace(pkc + ': float', pkc + ': int')

    @property
    def data(self):
        # TODO: should no longer be a property, we should be able to supply a query
        """
        Convert table connection into a blaze data object
        :return: blaze data object
        """
        if self.query:
            # table = self.source_db.connection.execute(query)
            #
            # # we take the first row, discover the dshape and modify it to a proper ds
            # dshape = str(discover(table.first())).replace('{', 'var * {')
            return bz.data(self.source_db.connection.execute(self.query))

        return bz.data(self.__table_uri, dshape=self.__dshape)

    @property
    def to_html(self):
        # TODO: this should no longer be a property
        df = odo(self.data.peek(), pd.DataFrame, dshape=self.data.dshape)
        try:
            return df.to_html().replace(
                '<table border="1" class="dataframe">',
                '<table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">'
            )

        except ValueError:
            return "Data unavailable"

    # @property
    # def to_df(self):
    #     # TODO:
    #     return dd.re(self.data)

    def transfer_to(self, target_db, start_from_id):
        pk_col = self.primary_key_column

        # TODO: using the original ref to the data causes a 'ValueError: No common leaves found in expressions'
        data = self.data
        odo(data[data[pk_col] >= start_from_id], target_db + "::" + self.name, dshape=data.dshape,
            keep_default_na=False)
