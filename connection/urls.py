from django.urls import path
from django.views.generic import TemplateView
import connection.views as views

app_name = 'connection'

urlpatterns = [
    path('', TemplateView.as_view(template_name='connection/type_select.html'), name='type_select'),
    path('<str:type>/create', views.connection_create, name='create'),
    path('<int:pk>/', views.connection_detail, name='detail'),
    path('<int:pk>/clear', views.tables_clear, name='tables_clear'),
    path('<int:pk>/delete', views.connection_delete, name='delete'),
    path('<int:pk>/update', views.connection_update, name='update'),
    path('table/<int:pk>/', views.loaded_tables, name='loaded_tables'),
    path('table/<int:pk>/add', views.table_add, name='table_add'),
    path('table/<int:pk>/~add', views.table_sql_add, name='table_sql_add'),
    path('table/<int:pk>/detail', views.table_detail, name='table_detail'),
    path('table/<int:pk>/delete', views.table_delete, name='table_delete'),

]
