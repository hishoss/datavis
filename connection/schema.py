import graphene
from graphene import relay, ObjectType
from graphene_django.types import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from .models import Connection, Table


class TableType(DjangoObjectType):
    columns = graphene.List(source='columns', of_type=graphene.String)
    just_name = graphene.String(source='just_name')
    column = graphene.List(source='column', of_type=graphene.Float)

    # column = relay.Node.Field(Table)

    # def resolve_column(self, column_name, **kwargs):
    #     return self.column(column_name)

    class Meta:
        model = Table
        filter_fields = {
            'name': ['exact', 'icontains'],
            'source_db': ['exact'],
            'source_db__name': ['exact', 'icontains', 'istartswith']
        }


class ConnectionsType(DjangoObjectType):
    class Meta:
        model = Connection
        only_fields = ('name', 'user', 'type', 'is_valid', 'updated', 'timestamp')
        filter_fields = {
            'name': ['exact']
        }


class Query:
    table = graphene.Field(TableType, id=graphene.Int(), name=graphene.String())
    all_tables = graphene.List(TableType)

    connection = graphene.Field(ConnectionsType)
    all_connections = graphene.Field(ConnectionsType)

    table_column = graphene.Field(TableType, id=graphene.Int(), column=graphene.String())

    def resolve_table_column(self, info, **kwargs):
        id = kwargs.get('id')
        column = kwargs.get('column')

        if id and column:
            try:
                col = Table.objects.get(id=id).column(column)
                return col
            except:
                return None

    def resolve_table(self, info, **kwargs):
        id = kwargs.get('id')
        name = kwargs.get('name')

        if id:
            return Table.objects.get(id=id)
        if name:
            return Table.objects.get(name=name)

        return None

    # @staticmethod
    def resolve_all_tables(self, info):
        if info.context.user.is_staff:
            return Table.objects.all()
        if info.context.user.is_authenticated:
            return Table.objects.filter(source_db__user=info.context.user)
        return Table.objects.none()

    def resolve_all_connections(self, info):
        if info.context.user.is_staff:
            return Connection.objects.all()  # Connection.objects.filter(is_valid=True)
        if info.context.user.is_authenticated:
            return Connection.objects.filter(user=info.context.user)
        return Connection.objects.none()
