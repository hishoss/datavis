from datetime import datetime, timedelta
from importlib import import_module
import json
from random import randint

from datavis.celery import app
from django.shortcuts import get_object_or_404
from django_celery_beat.models import PeriodicTask, IntervalSchedule

from sqlalchemy.exc import IntegrityError

from .app_settings import WAREHOUSE_DB_URL
from .models import Table


@app.task(name="commit_table")
def commit_table(tb_pk, db_url, destination='raw'):
    from .models import Table

    tb = get_object_or_404(Table, pk=tb_pk)
    primary_key_column = tb.primary_key_column

    # TODO: we need to ensure the primary_key_column is a unique integer field else we break
    # if df[primary_key_column].dtype != 'int':  # this is valid, pandas compares it properly
    #   message.warning(request, f"{primary_key_column} doesn't qualify to be a primary key column.")
    #   break
    # elif df[primary_key_column].duplicated().any() == True:
    #   message.warning(request, f"{primary_key_column} doesn't qualify to be a primary key column.")
    #   break

    # TODO: add a checkbox so we may add an id field using pandas indicies

    # -2 so on initial commit, if both min and max are none, they'll have the same value and we don't bother to commit
    last_id = int(tb.data[primary_key_column].max() or -2)

    # TODO: let a signal set the last committed_id immediately table is created
    # we do this because, on first consume, we want to set the right min id
    if tb.last_committed_id == -1:
        # This because, during first commit, last committed id is actually the first id, so we reduce it by 1
        tb.last_committed_id = int(tb.data[primary_key_column].min() or -1) - 1
        tb.save()

    last_committed_id = tb.last_committed_id

    if last_id > last_committed_id:
        if destination == 'warehouse':
            try:
                # TODO: something gone wrong with importing this
                preprocess = import_module(
                    f"ipynb.fs.defs.user_notebooks.{tb.source_db.user.username}.{tb.name}",
                    "Preprocessing").Preprocessing

                # TODO: do we clean from the initial index or strictly the new data?
                # TODO: how do we clean a blaze data object?
                # df = preprocess(df).clean()

            except (ImportError, ModuleNotFoundError):
                pass

        tb.transfer_to(db_url, last_committed_id+1)

        if destination == 'warehouse':
            tb.last_committed_id = last_id
            tb.save()

        return f"Data committed. lcid: {last_committed_id}"
    else:
        return "Nothing new to commit. lcid: {last_committed_id}"


def create_periodic_task(tb_pk, table_name):
    """This creates a periodic task of pulling data from source connection into warehouse
    :param tb_pk: table pk
    :param table_name: name of table to create
    :return: None
    """
    tb = Table.objects.get(pk=tb_pk)
    package = tb.source_db.user.package

    # TODO: if customer isn't subscribed to a package, we need to totally prevent this from happening. No package, error
    frequency = getattr(package, 'frequency', 3600)
    duration = getattr(package, 'duration', 1)
    schedule, _ = IntervalSchedule.objects.get_or_create(every=frequency, period=IntervalSchedule.MINUTES)

    # This could still fail if all args apart from name are different
    p, created = PeriodicTask.objects.get_or_create(
        interval=schedule,
        name='Transfer ' + table_name + ' to warehouse',
        task='commit_table',
        args=json.dumps([tb.pk, WAREHOUSE_DB_URL]),
        kwargs=json.dumps({'destination': 'warehouse'}),
    )

    if created:
        # enable the periodic task
        p.expires = datetime.utcnow() + timedelta(days=duration)
        p.enabled = True
        p.save()

    return
