from subprocess import Popen
from os import makedirs

from django_celery_beat.models import PeriodicTask
import nbformat as nbf

from .app_settings import MEDIA_ROOT, RAW_DB_URL


def make_db_table_name(con, table_name):
    return con.user.username + '_' + con.name + '__' + table_name


def delete_table(tb):
    # import os
    # from subprocess import Popen
    # from django_celery_beat.models import PeriodicTask

    tb.connection.execute(f"DROP TABLE IF EXISTS '{tb.name}';")
    tb.connection.execute('VACUUM')

    # try to delete table in other db i.e if connection is warehouse+, connection0 is rawDB

    tb.connection0.execute(f"DROP TABLE IF EXISTS '{tb.name}';")
    tb.connection0.execute('VACUUM')

    try:
        nb = MEDIA_ROOT + '/' + tb.source_db.user.username + '/notebooks/' + tb.name + '.ipynb'
        # nb = os.path.join(BASE_DIR, 'user_notebooks/' + tb.source_db.user.username + '/' + tb.name + '.ipynb')
        # from IPython import embed; embed()
        Popen(['rm', nb])
    except:
        pass

    if tb.state == 'cleaned':
        pname = "Transfer " + tb.just_name + " to warehouse"
        task = PeriodicTask.objects.filter(name=pname).first()
        try:
            task.delete()
        except AttributeError:
            pass

    tb.delete()


def delete_tables(con_pk):
    from connection.models import Connection

    # first just because result is a singleton queryset and we just want the object
    con = Connection.objects.filter(pk=con_pk).first()
    tbs = con.tables.all()
    if tbs:
        for tb in tbs:
            delete_table(tb)
    return 1


def db_upload_path(instance, filename):
    return 'db_uploads/{0}/{1}'.format(instance.user.username, filename)


def tables_loaded(con):
    # get all tables associated with connection and take out the prefix i.e 'name_dbname.'
    # then map over the triples of pk, db_source_id, and tb_name and return (tb_name, pk)
    prefix = con.user.username + '_' + con.name + '__'
    return dict(map(lambda tb: (tb[2].replace(prefix, ''), tb[0]), con.tables.all().values_list()))


def create_notebook(username, table_name):
    # from os import makedirs
    # import nbformat as nbf
    # from .app_settings import RAW_DB_URL

    text0 = f"""## AD HOC ANALYSIS and PRE-PROCESSING of {table_name}
This is where we clean the data and do some ad-hoc analysis on the data before we send it to the warehouse. 
df is the dataframe containing the data to be cleaned.

## Before you continue:
click on the 'Not Trusted' button on the top right corner of this notebook and set it to trusted.
"""

    code0 = f"""import dask.dataframe as dd
from numpy import NaN
import sqlalchemy as sq

engine = sq.create_engine('{RAW_DB_URL}')
df = dd.read_sql_table('{table_name}', engine)
engine.dispose()"""

    text1 = """## Template for the preprocessing class. 
Add all the steps to clean to the clean method and make sure it returns a dataframe."""

    code1 = """EG: 

        
class Preprocessing:
    def __init__(self, df):
        self.df = df

    def clean(self):
        # eg.
        # df = self.df.replace(['?', 'null', 'nan'], NaN)
        # return df
        pass"""

    text2 = """## Finally, go to the backend and change the state of the table to 'cleaned'."""

    # dirr = "./user_notebooks/" + username + "/"
    dir1 = MEDIA_ROOT + '/' + username + '/'
    dir2 = dir1 + 'notebooks/'

    try:
        makedirs(dir2)
    except FileExistsError:
        pass

    # Create an init file in 'username' and 'notebooks' folders
    for d in [dir1, dir2]:
        if '__init__.py' not in d:
            with open(d + '__init__.py', 'w') as f:
                f.write('')

    nb = nbf.v4.new_notebook()
    nb['cells'] = [nbf.v4.new_markdown_cell(text0),
                   nbf.v4.new_code_cell(code0),
                   nbf.v4.new_markdown_cell(text1),
                   nbf.v4.new_code_cell(code1),
                   nbf.v4.new_code_cell(),
                   nbf.v4.new_code_cell(),
                   nbf.v4.new_markdown_cell(text2),
                   ]

    with open(dir2 + table_name + ".ipynb", 'w') as f:
        nbf.write(nb, f)

    return 1


def extract_jupyter_url(logfile):
    import re

    with open(logfile, 'r') as f:
        log = f.read()
        f.close()

    urls = re.findall("(?P<url>https?://[^\s]+)", log)
    return urls[-1]
