import blaze
# import numpy
from sympy.parsing.sympy_parser import eval_expr

from django.db import models

# numpy_dict = {a: getattr(numpy, a) for a in dir(numpy)}
blaze_dict = {a: getattr(blaze, a) for a in dir(blaze)}


class Metric(models.Model):
    name = models.CharField(max_length=255)
    formula = models.CharField(max_length=255)
    variables = models.CharField(max_length=255, help_text="Variables used in the formula.")
    description = models.TextField()
    updated = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.name} : {self.short_name}"

    @property
    def short_name(self):
        words = self.name.split()
        if len(words) > 1:
            return ''.join([w[0].lower() for w in words])
        else:
            return words[0].lower() + words[-1].lower()

    @property
    def variable_mappings(self):
        """
        Helper to facilitate filling up of variables
        :return: dictionary of terms in the formula
        """
        return {key: None for key in map(lambda x: x.strip(), self.variables.split(','))}

    def compute(self, variables):
        """
        Compute metric from given dictionary of variables
        :param variables: dict of variables with values(np.arrays) within formula
        :return: float value of formula. If return type is None, then either variables enough or correct
        """
        try:
            return {self.short_name: eval_expr(self.formula, variables, blaze_dict)}
        except NameError:
            return "Variable missing"


class KPI(models.Model):
    name = models.CharField(max_length=255)
    metrics = models.ManyToManyField(Metric)
    formula = models.CharField(max_length=255)
    # variables = models.CharField(max_length=255, help_text="Variables used in the formula.")
    # functions = models.CharField(max_length=255, blank=True, null=True, help_text="Functions used in the formula.")
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'KPI'
        verbose_name_plural = 'KPIs'

    def __str__(self):
        return self.name

    @property
    def variable_mappings(self):
        """
        :return: dictionary of terms in the formula of each metric present in the kpi
        """
        r = {}
        for m in self.metrics.all():
            r = {**r, **m.variable_mappings}
        return r
        # return dict(reduce(lambda old, new: old | new.variable_mappings.items(), self.metrics.all(), {}))

    def compute(self, variables):
        """
        Compute metric from given dictionary of variables
        :param variables: dict of variables with values(np.arrays) within formula
        :return: float value of formula. If return type is None, then either variables enough or correct
        """
        try:
            computed_metrics = [m.compute(variables) for m in self.metrics.all()]
        except (TypeError, IndexError):
            return "Variables must be a dictionary"

        kpi_vars = {}
        for m in computed_metrics:
            kpi_vars = {**kpi_vars, **m}

        try:
            return eval_expr(self.formula, kpi_vars, blaze_dict)
        except NameError:
            return "Some variables not present"
