from django.contrib import admin

from .models import KPI, Metric


class KPIAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_filter = ('updated',)


class MetricAdmin(admin.ModelAdmin):
    list_display = ('name', 'short_name', 'description')
    list_filter = ('updated',)


admin.site.register(KPI, KPIAdmin)
admin.site.register(Metric, MetricAdmin)
